use std::fs;
use std::path::Path;

pub fn delete_files_in_folder(folder_path: &Path) -> Result<(), std::io::Error> {
    for entry in fs::read_dir(folder_path)? {
        let entry = entry?;
        let file_path = entry.path();

        if file_path.is_file() {
            match fs::remove_file(&file_path) {
                Ok(()) => println!("Deleted file: {:?}", file_path),
                Err(e) => eprintln!("Error deleting file: {:?}", e),
            }
        }

        if file_path.is_dir() {
            match fs::remove_dir_all(&file_path) {
                Ok(()) => println!("Deleted file: {:?}", file_path),
                Err(e) => eprintln!("Error director file: {:?}", e),
            }
        }
    }
    Ok(())
}
