mod delete_file;
use std::env;
use std::path::Path;
fn main() {
    let variable_name = "temp";
    match env::var(variable_name) {
        Ok(value) => {
            let variable_value = value;
            println!("The value of {} is {}", variable_name, variable_value);
            let folder_path = Path::new(&variable_value);
            match delete_file::delete_files_in_folder(&folder_path) {
                Ok(()) => println!("All files deleted successfully!"),
                Err(e) => eprintln!("Error deleting files: {}", e),
            }
        }
        Err(e) => eprintln!("Error accessing environment variable: {}", e),
    }
}
